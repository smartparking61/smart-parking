EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:+1v2
LIBS:+1v8
LIBS:+3v
LIBS:+3v3
LIBS:+5v
LIBS:+12v
LIBS:+15v
LIBS:+vs
LIBS:2-spst
LIBS:-15v
LIBS:74avc4t245
LIBS:74lvc1g
LIBS:74vc1t45
LIBS:74vc2t45
LIBS:aat1217
LIBS:aat3681a
LIBS:acs711ex
LIBS:ad8429
LIBS:ant
LIBS:aoz128x
LIBS:AP3402
LIBS:bat
LIBS:bq5105xb
LIBS:bt51013b
LIBS:buck-sot23-5
LIBS:buck-sot23-6
LIBS:button-dpdt
LIBS:c
LIBS:cm1624
LIBS:com
LIBS:conn-1
LIBS:conn-2
LIBS:conn-2x4
LIBS:conn-2x5
LIBS:conn-3
LIBS:conn-4
LIBS:conn-5
LIBS:conn-6
LIBS:conn-7
LIBS:conn-8
LIBS:conn-10
LIBS:cp
LIBS:d
LIBS:de-9
LIBS:debugheader
LIBS:drdc3105
LIBS:drv883x
LIBS:drv8838
LIBS:drv8839
LIBS:dtc-npn
LIBS:erc12864-1
LIBS:esp-12
LIBS:ferrite
LIBS:fuse
LIBS:gnd
LIBS:ICE40HX4K-144
LIBS:ICM-20689
LIBS:ina199
LIBS:in-amp
LIBS:ip425xcz12
LIBS:isp130301
LIBS:jtag
LIBS:jumper
LIBS:k22f-64lqfp
LIBS:l
LIBS:lcd5110
LIBS:ldo-sot-23-5
LIBS:led
LIBS:lis2-lga12
LIBS:lm78xx-so8
LIBS:lm3671
LIBS:lm27313
LIBS:ln1410
LIBS:lpc812
LIBS:lpc824
LIBS:lpddrx16
LIBS:lt6102
LIBS:lt6106
LIBS:ltc3251
LIBS:ltc3260
LIBS:mag3110
LIBS:mag3110-cache
LIBS:max4737eud
LIBS:max6070
LIBS:max9938
LIBS:mchck
LIBS:mcp47x6-sot-23-6
LIBS:mcp3221
LIBS:mic5205
LIBS:mic5365
LIBS:mic9409x
LIBS:mic23031
LIBS:microsd
LIBS:mkl03zxxvfg4
LIBS:mkl03zxxvfk4
LIBS:MKL26ZxxVFM4
LIBS:mkl27zxxvfm4
LIBS:mounthole
LIBS:mounting-hole
LIBS:mpl3115a2
LIBS:mpu6050
LIBS:mpu-6050
LIBS:mun53
LIBS:nmos
LIBS:nmos-pmos-sot23-6
LIBS:nmos-pmos-sot-666
LIBS:npn
LIBS:nup4114
LIBS:op-amp
LIBS:phone-3
LIBS:pmos
LIBS:pnp
LIBS:pot
LIBS:power_flag
LIBS:powersyms
LIBS:pwr
LIBS:r
LIBS:r4
LIBS:relay
LIBS:rs485trx-fullduplex
LIBS:SAM3U-144
LIBS:schottky
LIBS:si8410
LIBS:sip32401a
LIBS:sit1602
LIBS:slide-spdt
LIBS:solder-jumper-nc
LIBS:spiflash
LIBS:tactile-2
LIBS:tactile-4
LIBS:test
LIBS:testpoint
LIBS:thermistor
LIBS:tlv713
LIBS:TMC2130
LIBS:tp4055
LIBS:tp4056
LIBS:tpd4s012
LIBS:tps6040x
LIBS:tps27081a
LIBS:ts3usb31
LIBS:usb
LIBS:usb334x
LIBS:USB3300
LIBS:usb-c
LIBS:vana
LIBS:varef
LIBS:vbu
LIBS:vcore
LIBS:vin
LIBS:vio
LIBS:vout
LIBS:vpll
LIBS:-vs
LIBS:vtarget
LIBS:vusb
LIBS:vutmi
LIBS:xtal
LIBS:zener
LIBS:Mag3110-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAG3110 U1
U 1 1 5B353C34
P 5500 3850
F 0 "U1" H 5200 3550 60  0000 L CNN
F 1 "MAG3110" H 5550 3550 60  0000 L CNN
F 2 "Housings_DFN_QFN:DFN-10_2x2mm_Pitch0.4mm" H 2500 4000 60  0001 C CNN
F 3 "" H 2500 4000 60  0000 C CNN
	1    5500 3850
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5B353C97
P 4050 2200
F 0 "C2" H 4075 2300 50  0000 L CNN
F 1 "1uF" H 4075 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4088 2050 50  0001 C CNN
F 3 "" H 4050 2200 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5B353DA6
P 4550 2200
F 0 "C3" H 4575 2300 50  0000 L CNN
F 1 "10nF" H 4575 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4588 2050 50  0001 C CNN
F 3 "" H 4550 2200 50  0001 C CNN
	1    4550 2200
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5B353DCF
P 6400 4400
F 0 "C4" H 6425 4500 50  0000 L CNN
F 1 "10nF" H 6425 4300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6438 4250 50  0001 C CNN
F 3 "" H 6400 4400 50  0001 C CNN
	1    6400 4400
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5B353E3D
P 6900 4400
F 0 "C5" H 6925 4500 50  0000 L CNN
F 1 "10nF" H 6925 4300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6938 4250 50  0001 C CNN
F 3 "" H 6900 4400 50  0001 C CNN
	1    6900 4400
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B353E6C
P 3250 3350
F 0 "C1" H 3275 3450 50  0000 L CNN
F 1 "10nF" H 3275 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3288 3200 50  0001 C CNN
F 3 "" H 3250 3350 50  0001 C CNN
	1    3250 3350
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B353EE2
P 4150 2900
F 0 "R1" V 4230 2900 50  0000 C CNN
F 1 "10k" V 4150 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4080 2900 50  0001 C CNN
F 3 "" H 4150 2900 50  0001 C CNN
	1    4150 2900
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B353FE4
P 4550 2900
F 0 "R2" V 4630 2900 50  0000 C CNN
F 1 "10k" V 4550 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4480 2900 50  0001 C CNN
F 3 "" H 4550 2900 50  0001 C CNN
	1    4550 2900
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR01
U 1 1 5B35404E
P 4050 2350
F 0 "#PWR01" H 4050 2100 50  0001 C CNN
F 1 "Earth" H 4050 2200 50  0001 C CNN
F 2 "" H 4050 2350 50  0001 C CNN
F 3 "" H 4050 2350 50  0001 C CNN
	1    4050 2350
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR02
U 1 1 5B354078
P 4550 2350
F 0 "#PWR02" H 4550 2100 50  0001 C CNN
F 1 "Earth" H 4550 2200 50  0001 C CNN
F 2 "" H 4550 2350 50  0001 C CNN
F 3 "" H 4550 2350 50  0001 C CNN
	1    4550 2350
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR03
U 1 1 5B35416B
P 3250 3500
F 0 "#PWR03" H 3250 3250 50  0001 C CNN
F 1 "Earth" H 3250 3350 50  0001 C CNN
F 2 "" H 3250 3500 50  0001 C CNN
F 3 "" H 3250 3500 50  0001 C CNN
	1    3250 3500
	1    0    0    -1  
$EndComp
Text GLabel 4000 3600 0    60   BiDi ~ 0
SDA
Text GLabel 3750 3700 0    60   Input ~ 0
SCL
$Comp
L Earth #PWR04
U 1 1 5B3545E9
P 5500 4150
F 0 "#PWR04" H 5500 3900 50  0001 C CNN
F 1 "Earth" H 5500 4000 50  0001 C CNN
F 2 "" H 5500 4150 50  0001 C CNN
F 3 "" H 5500 4150 50  0001 C CNN
	1    5500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2050 5500 3400
Wire Wire Line
	4050 2050 5500 2050
Connection ~ 4550 2050
Wire Wire Line
	5600 2750 5600 3400
Wire Wire Line
	3250 2750 5600 2750
Wire Wire Line
	3250 2750 3250 3200
Wire Wire Line
	4550 3050 4550 3600
Wire Wire Line
	4000 3600 5100 3600
Wire Wire Line
	4150 3050 4150 3700
Wire Wire Line
	3750 3700 5100 3700
Connection ~ 4550 3600
Connection ~ 4150 3700
Wire Wire Line
	5400 4150 5500 4150
Wire Wire Line
	6000 3950 6400 3950
Wire Wire Line
	6400 3950 6400 4250
Wire Wire Line
	6000 3850 6900 3850
Wire Wire Line
	6900 3850 6900 4250
$Comp
L Earth #PWR05
U 1 1 5B35465B
P 6400 4550
F 0 "#PWR05" H 6400 4300 50  0001 C CNN
F 1 "Earth" H 6400 4400 50  0001 C CNN
F 2 "" H 6400 4550 50  0001 C CNN
F 3 "" H 6400 4550 50  0001 C CNN
	1    6400 4550
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR06
U 1 1 5B35467E
P 6900 4550
F 0 "#PWR06" H 6900 4300 50  0001 C CNN
F 1 "Earth" H 6900 4400 50  0001 C CNN
F 2 "" H 6900 4550 50  0001 C CNN
F 3 "" H 6900 4550 50  0001 C CNN
	1    6900 4550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
