PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
FSOS8700CQ_PKG
$EndINDEX
$MODULE FSOS8700CQ_PKG
Po 0 0 0 15 00000000 00000000 ~~
Li FSOS8700CQ_PKG
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0 0 1 0.9 0 0.05 N H 21 "FSOS8700CQ_PKG"
T1 0 0 1 0.9 0 0.05 N H 21 "VAL**"
DS -1.5 1.5 -1.5 -1.5 0.127 21
DS -1.5 -1.5 1.5 -1.5 0.127 21
DS 1.5 -1.5 1.5 1.5 0.127 21
DS 1.5 1.5 -1.5 1.5 0.127 21
DC -1.83 -1.95 -1.45517 -1.95 0.127 21
DP 0 0 0 0 4 0.381 24
Dl -0.850663 -0.83
Dl 0.84 -0.83
Dl 0.84 0.840655
Dl -0.850663 0.840655
$PAD
Sh "P$1" R 0.8 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.28 -1
$EndPAD
$PAD
Sh "P$2" R 0.8 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.28 -0.5
$EndPAD
$PAD
Sh "P$3" R 0.8 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.28 0
$EndPAD
$PAD
Sh "P$4" R 0.8 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.28 0.5
$EndPAD
$PAD
Sh "P$5" R 0.8 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.28 1
$EndPAD
$PAD
Sh "P$6" R 0.8 0.3 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 1.28
$EndPAD
$PAD
Sh "P$7" R 0.8 0.3 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 1.28
$EndPAD
$PAD
Sh "P$8" R 0.8 0.3 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 1.28
$EndPAD
$PAD
Sh "P$9" R 0.8 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.28 1
$EndPAD
$PAD
Sh "P$10" R 0.8 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.28 0.5
$EndPAD
$PAD
Sh "P$11" R 0.8 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.28 0
$EndPAD
$PAD
Sh "P$12" R 0.8 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.28 -0.5
$EndPAD
$PAD
Sh "P$13" R 0.8 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.28 -1
$EndPAD
$PAD
Sh "P$14" R 0.8 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 -1.28
$EndPAD
$PAD
Sh "P$15" R 0.8 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 -1.28
$EndPAD
$PAD
Sh "P$16" R 0.8 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 -1.28
$EndPAD
$EndMODULE FSOS8700CQ_PKG
