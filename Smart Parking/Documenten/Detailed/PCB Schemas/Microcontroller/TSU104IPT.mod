PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOP65P640X120-14N
$EndINDEX
$MODULE SOP65P640X120-14N
Po 0 0 0 15 00000000 00000000 ~~
Li SOP65P640X120-14N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -0.456203 -4.02009 1.12248 1.12248 0 0.05 N V 21 "SOP65P640X120-14N"
T1 -0.0441649 3.8035 1.12435 1.12435 0 0.05 N V 21 "VAL**"
DS -2.25 2.55 -2.25 -2.55 0.127 24
DS -2.25 -2.55 2.25 -2.55 0.127 24
DS 2.25 -2.55 2.25 2.55 0.127 24
DS 2.25 2.55 -2.25 2.55 0.127 24
DS 2.25 2.55 -2.25 2.55 0.127 21
DS 2.25 -2.55 -2.25 -2.55 0.127 21
DC -4.092 -1.95 -3.992 -1.95 0.2 21
DS -3.91 -2.8 3.91 -2.8 0.05 24
DS 3.91 -2.8 3.91 2.8 0.05 24
DS 3.91 2.8 -3.91 2.8 0.05 24
DS -3.91 2.8 -3.91 -2.8 0.05 24
$PAD
Sh "1" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 -1.95
$EndPAD
$PAD
Sh "2" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 -1.3
$EndPAD
$PAD
Sh "3" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 -0.65
$EndPAD
$PAD
Sh "4" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 0
$EndPAD
$PAD
Sh "5" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 0.65
$EndPAD
$PAD
Sh "6" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 1.3
$EndPAD
$PAD
Sh "7" R 1.56 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.88 1.95
$EndPAD
$PAD
Sh "8" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 1.95
$EndPAD
$PAD
Sh "9" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 1.3
$EndPAD
$PAD
Sh "10" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 0.65
$EndPAD
$PAD
Sh "11" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 0
$EndPAD
$PAD
Sh "12" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 -0.65
$EndPAD
$PAD
Sh "13" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 -1.3
$EndPAD
$PAD
Sh "14" R 1.56 0.41 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.88 -1.95
$EndPAD
$EndMODULE SOP65P640X120-14N
