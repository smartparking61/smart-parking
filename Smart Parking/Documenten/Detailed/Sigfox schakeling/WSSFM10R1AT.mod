PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
XCVR_WSSFM10R1AT
$EndINDEX
$MODULE XCVR_WSSFM10R1AT
Po 0 0 0 15 00000000 00000000 ~~
Li XCVR_WSSFM10R1AT
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -5.5436 -8.07344 0.48111 0.48111 0 0.05 N V 21 "XCVR_WSSFM10R1AT"
T1 -5.12847 8.24919 0.480232 0.480232 0 0.05 N V 21 "VAL**"
DS -6.5 -7.5 6.5 -7.5 0.127 21
DS 6.5 -7.5 6.5 7.5 0.127 24
DS 6.5 7.5 -6.5 7.5 0.127 24
DS -6.5 7.5 -6.5 -7.5 0.127 24
DS -6.75 -7.75 6.75 -7.75 0.05 24
DS 6.75 -7.75 6.75 7.75 0.05 24
DC -7.25 -6 -7.05 -6 0 21
DS -6.75 7.75 -6.75 -7.75 0.05 24
DS -6.75 7.75 6.75 7.75 0.05 24
DS -6.5 7 -6.5 7.5 0.127 21
DS -6.5 7.5 -4.2 7.5 0.127 21
DS 6.5 7 6.5 7.5 0.127 21
DS 6.5 7.5 4.2 7.5 0.127 21
$PAD
Sh "1" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -6.05
$EndPAD
$PAD
Sh "2" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -4.95
$EndPAD
$PAD
Sh "3" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -3.85
$EndPAD
$PAD
Sh "4" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -2.75
$EndPAD
$PAD
Sh "5" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -1.65
$EndPAD
$PAD
Sh "6" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 -0.55
$EndPAD
$PAD
Sh "7" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 0.55
$EndPAD
$PAD
Sh "8" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 1.65
$EndPAD
$PAD
Sh "9" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 2.75
$EndPAD
$PAD
Sh "10" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 3.85
$EndPAD
$PAD
Sh "11" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 4.95
$EndPAD
$PAD
Sh "12" R 1.15 0.81 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.9 6.05
$EndPAD
$PAD
Sh "13" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.3 6.9
$EndPAD
$PAD
Sh "14" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 6.9
$EndPAD
$PAD
Sh "15" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.1 6.9
$EndPAD
$PAD
Sh "16" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 6.9
$EndPAD
$PAD
Sh "17" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.1 6.9
$EndPAD
$PAD
Sh "18" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 6.9
$EndPAD
$PAD
Sh "19" R 1.15 0.81 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.3 6.9
$EndPAD
$PAD
Sh "20" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 6.05
$EndPAD
$PAD
Sh "21" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 4.95
$EndPAD
$PAD
Sh "22" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 3.85
$EndPAD
$PAD
Sh "23" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 2.75
$EndPAD
$PAD
Sh "24" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 1.65
$EndPAD
$PAD
Sh "25" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 0.55
$EndPAD
$PAD
Sh "26" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -0.55
$EndPAD
$PAD
Sh "27" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -1.65
$EndPAD
$PAD
Sh "28" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -2.75
$EndPAD
$PAD
Sh "29" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -3.85
$EndPAD
$PAD
Sh "30" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -4.95
$EndPAD
$PAD
Sh "31" R 1.15 0.81 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.9 -6.05
$EndPAD
$EndMODULE XCVR_WSSFM10R1AT
