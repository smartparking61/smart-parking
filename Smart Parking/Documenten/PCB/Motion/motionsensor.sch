EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:tsu104
LIBS:motionsensor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 1650 3050 0    60   ~ 0
Drain
Text Label 1650 3200 0    60   ~ 0
Source
Text Label 1650 3350 0    60   ~ 0
Gnd
$Comp
L GND #PWR?
U 1 1 5ABCBF2B
P 1950 3450
F 0 "#PWR?" H 1950 3200 50  0001 C CNN
F 1 "GND" H 1950 3300 50  0000 C CNN
F 2 "" H 1950 3450 50  0001 C CNN
F 3 "" H 1950 3450 50  0001 C CNN
	1    1950 3450
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 5ABCBF42
P 1950 2850
F 0 "#PWR?" H 1950 2700 50  0001 C CNN
F 1 "VCC" H 1950 3000 50  0000 C CNN
F 2 "" H 1950 2850 50  0001 C CNN
F 3 "" H 1950 2850 50  0001 C CNN
	1    1950 2850
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5ABCBF7C
P 2300 3350
F 0 "R?" V 2380 3350 50  0000 C CNN
F 1 "56K" V 2300 3350 50  0000 C CNN
F 2 "" V 2230 3350 50  0001 C CNN
F 3 "" H 2300 3350 50  0001 C CNN
	1    2300 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCBFCA
P 2300 3500
F 0 "#PWR?" H 2300 3250 50  0001 C CNN
F 1 "GND" H 2300 3350 50  0000 C CNN
F 2 "" H 2300 3500 50  0001 C CNN
F 3 "" H 2300 3500 50  0001 C CNN
	1    2300 3500
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5ABCC0F3
P 2850 3100
F 0 "R?" V 2930 3100 50  0000 C CNN
F 1 "13K" V 2850 3100 50  0000 C CNN
F 2 "" V 2780 3100 50  0001 C CNN
F 3 "" H 2850 3100 50  0001 C CNN
	1    2850 3100
	0    -1   -1   0   
$EndComp
$Comp
L R R?
U 1 1 5ABCC13A
P 2950 4000
F 0 "R?" V 3030 4000 50  0000 C CNN
F 1 "470K" V 2950 4000 50  0000 C CNN
F 2 "" V 2880 4000 50  0001 C CNN
F 3 "" H 2950 4000 50  0001 C CNN
	1    2950 4000
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5ABCC145
P 2600 3350
F 0 "C?" H 2625 3450 50  0000 L CNN
F 1 "3.3n" H 2625 3250 50  0000 L CNN
F 2 "" H 2638 3200 50  0001 C CNN
F 3 "" H 2600 3350 50  0001 C CNN
	1    2600 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCC27F
P 2600 3500
F 0 "#PWR?" H 2600 3250 50  0001 C CNN
F 1 "GND" H 2600 3350 50  0000 C CNN
F 2 "" H 2600 3500 50  0001 C CNN
F 3 "" H 2600 3500 50  0001 C CNN
	1    2600 3500
	1    0    0    -1  
$EndComp
$Comp
L TSU104 U?
U 1 1 5ABCD060
P 3750 3250
F 0 "U?" H 3900 3650 60  0000 C CNN
F 1 "TSU104" H 3750 2700 60  0000 C CNN
F 2 "" H 3900 3650 60  0001 C CNN
F 3 "" H 3900 3650 60  0001 C CNN
	1    3750 3250
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5ABCD203
P 2550 3100
F 0 "C?" H 2575 3200 50  0000 L CNN
F 1 "22u" H 2575 3000 50  0000 L CNN
F 2 "" H 2588 2950 50  0001 C CNN
F 3 "" H 2550 3100 50  0001 C CNN
	1    2550 3100
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCD276
P 2400 3100
F 0 "#PWR?" H 2400 2850 50  0001 C CNN
F 1 "GND" H 2400 2950 50  0000 C CNN
F 2 "" H 2400 3100 50  0001 C CNN
F 3 "" H 2400 3100 50  0001 C CNN
	1    2400 3100
	0    1    1    0   
$EndComp
$Comp
L C C?
U 1 1 5ABCD689
P 3200 2550
F 0 "C?" H 3225 2650 50  0000 L CNN
F 1 "47n" H 3225 2450 50  0000 L CNN
F 2 "" H 3238 2400 50  0001 C CNN
F 3 "" H 3200 2550 50  0001 C CNN
	1    3200 2550
	0    -1   -1   0   
$EndComp
$Comp
L R R?
U 1 1 5ABCD825
P 3200 2300
F 0 "R?" V 3280 2300 50  0000 C CNN
F 1 "680K" V 3200 2300 50  0000 C CNN
F 2 "" V 3130 2300 50  0001 C CNN
F 3 "" H 3200 2300 50  0001 C CNN
	1    3200 2300
	0    1    1    0   
$EndComp
$Comp
L D D?
U 1 1 5ABCDB0D
P 3200 2100
F 0 "D?" H 3200 2200 50  0000 C CNN
F 1 "D" H 3200 2000 50  0000 C CNN
F 2 "" H 3200 2100 50  0001 C CNN
F 3 "" H 3200 2100 50  0001 C CNN
	1    3200 2100
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5ABCDDCA
P 3950 2750
F 0 "R?" V 4030 2750 50  0000 C CNN
F 1 "13K" V 3950 2750 50  0000 C CNN
F 2 "" V 3880 2750 50  0001 C CNN
F 3 "" H 3950 2750 50  0001 C CNN
	1    3950 2750
	0    -1   -1   0   
$EndComp
$Comp
L C C?
U 1 1 5ABCDDD0
P 3650 2750
F 0 "C?" H 3675 2850 50  0000 L CNN
F 1 "22u" H 3675 2650 50  0000 L CNN
F 2 "" H 3688 2600 50  0001 C CNN
F 3 "" H 3650 2750 50  0001 C CNN
	1    3650 2750
	0    -1   -1   0   
$EndComp
$Comp
L C C?
U 1 1 5ABCE21A
P 4700 2750
F 0 "C?" H 4725 2850 50  0000 L CNN
F 1 "47n" H 4725 2650 50  0000 L CNN
F 2 "" H 4738 2600 50  0001 C CNN
F 3 "" H 4700 2750 50  0001 C CNN
	1    4700 2750
	0    -1   -1   0   
$EndComp
$Comp
L R R?
U 1 1 5ABCE220
P 4700 2550
F 0 "R?" V 4780 2550 50  0000 C CNN
F 1 "680K" V 4700 2550 50  0000 C CNN
F 2 "" V 4630 2550 50  0001 C CNN
F 3 "" H 4700 2550 50  0001 C CNN
	1    4700 2550
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR?
U 1 1 5ABCF262
P 3300 3300
F 0 "#PWR?" H 3300 3150 50  0001 C CNN
F 1 "VCC" H 3300 3450 50  0000 C CNN
F 2 "" H 3300 3300 50  0001 C CNN
F 3 "" H 3300 3300 50  0001 C CNN
	1    3300 3300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCF2AE
P 4350 3300
F 0 "#PWR?" H 4350 3050 50  0001 C CNN
F 1 "GND" H 4350 3150 50  0000 C CNN
F 2 "" H 4350 3300 50  0001 C CNN
F 3 "" H 4350 3300 50  0001 C CNN
	1    4350 3300
	0    -1   -1   0   
$EndComp
$Comp
L R R?
U 1 1 5ABCF3EC
P 4300 4350
F 0 "R?" V 4380 4350 50  0000 C CNN
F 1 "220K" V 4300 4350 50  0000 C CNN
F 2 "" V 4230 4350 50  0001 C CNN
F 3 "" H 4300 4350 50  0001 C CNN
	1    4300 4350
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5ABCF3F2
P 4050 4350
F 0 "C?" H 4075 4450 50  0000 L CNN
F 1 "10n" H 4075 4250 50  0000 L CNN
F 2 "" H 4088 4200 50  0001 C CNN
F 3 "" H 4050 4350 50  0001 C CNN
	1    4050 4350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCF4E4
P 4300 4500
F 0 "#PWR?" H 4300 4250 50  0001 C CNN
F 1 "GND" H 4300 4350 50  0000 C CNN
F 2 "" H 4300 4500 50  0001 C CNN
F 3 "" H 4300 4500 50  0001 C CNN
	1    4300 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ABCF512
P 4050 4500
F 0 "#PWR?" H 4050 4250 50  0001 C CNN
F 1 "GND" H 4050 4350 50  0000 C CNN
F 2 "" H 4050 4500 50  0001 C CNN
F 3 "" H 4050 4500 50  0001 C CNN
	1    4050 4500
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5ABCF5ED
P 2950 4400
F 0 "R?" V 3030 4400 50  0000 C CNN
F 1 "470K" V 2950 4400 50  0000 C CNN
F 2 "" V 2880 4400 50  0001 C CNN
F 3 "" H 2950 4400 50  0001 C CNN
	1    2950 4400
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5ABCF7C4
P 2800 3800
F 0 "R?" V 2880 3800 50  0000 C CNN
F 1 "220K" V 2800 3800 50  0000 C CNN
F 2 "" V 2730 3800 50  0001 C CNN
F 3 "" H 2800 3800 50  0001 C CNN
	1    2800 3800
	0    -1   -1   0   
$EndComp
$Comp
L VCC #PWR?
U 1 1 5ABCF8AD
P 2650 3800
F 0 "#PWR?" H 2650 3650 50  0001 C CNN
F 1 "VCC" H 2650 3950 50  0000 C CNN
F 2 "" H 2650 3800 50  0001 C CNN
F 3 "" H 2650 3800 50  0001 C CNN
	1    2650 3800
	0    -1   -1   0   
$EndComp
Text Label 6800 3600 0    60   ~ 0
VoutL
Text Label 6800 3850 0    60   ~ 0
VoutH
Connection ~ 4050 4200
Wire Wire Line
	3600 4550 2950 4550
Wire Wire Line
	3600 4200 3600 4550
Connection ~ 4300 4200
Connection ~ 4450 4200
Wire Wire Line
	4450 4200 4450 3500
Wire Wire Line
	2950 3400 3300 3400
Wire Wire Line
	2950 3400 2950 3850
Wire Wire Line
	2950 4250 2950 4150
Wire Wire Line
	4450 3500 4350 3500
Wire Wire Line
	3150 3500 3300 3500
Wire Wire Line
	3150 4000 3150 3500
Connection ~ 5100 4000
Wire Wire Line
	5100 3400 4350 3400
Wire Wire Line
	5100 4000 5100 3400
Wire Wire Line
	6200 4000 3150 4000
Wire Wire Line
	6200 3000 6200 4000
Connection ~ 4850 3000
Wire Wire Line
	4850 2550 4850 3000
Connection ~ 4550 2750
Wire Wire Line
	5600 3200 4350 3200
Wire Wire Line
	4350 3000 6200 3000
Wire Wire Line
	4550 3100 4350 3100
Wire Wire Line
	4550 2550 4550 3100
Wire Wire Line
	4100 2750 4550 2750
Wire Wire Line
	3300 2750 3500 2750
Connection ~ 3350 2750
Wire Wire Line
	3350 2100 3350 2750
Connection ~ 3050 3100
Wire Wire Line
	3050 2100 3050 3100
Wire Wire Line
	3300 3000 3300 2750
Wire Wire Line
	3000 3100 3300 3100
Connection ~ 2300 3200
Wire Wire Line
	1950 3350 1950 3450
Wire Wire Line
	1950 3050 1950 2850
Wire Wire Line
	1650 3350 1950 3350
Wire Wire Line
	1650 3200 3300 3200
Wire Wire Line
	1650 3050 1950 3050
Wire Wire Line
	4350 3600 6800 3600
Wire Wire Line
	3300 3600 3300 3850
Wire Wire Line
	3300 3850 6800 3850
Wire Wire Line
	3600 4200 4450 4200
Wire Wire Line
	2950 4200 2750 4200
Wire Wire Line
	2750 4200 2750 5000
Wire Wire Line
	2750 5000 5600 5000
Wire Wire Line
	5600 5000 5600 3200
Connection ~ 2950 4200
$EndSCHEMATC
