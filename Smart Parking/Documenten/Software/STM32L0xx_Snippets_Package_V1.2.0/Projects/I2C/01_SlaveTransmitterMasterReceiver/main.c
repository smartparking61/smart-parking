#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "rpi-armtimer.h"
#include "rpi-systimer.h"
#include "rpi-interrupts.h"

extern volatile bool Interrupt_is_working;
void enable_interrupts();

void main()
{
	RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
	RPI_ArmTimerInit();
	enable_interrupts();
	while(1)
	{
		if(Interrupt_is_working == true){
			printf("Interrupt werkt\n");
		}
	}
}

void enable_interrupts()
{
	__asm__{
		"mrs r0, cpsr"
		"bic r0, r0, #0x80"
		"msr cpsr_c, r0"
		"mov pc, lr"
	}
}