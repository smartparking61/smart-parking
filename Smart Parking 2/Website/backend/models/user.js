const mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
    username: {type: String, index: true, unique: true},
    password: {type: String}
});
module.exports.schema = UserSchema;
module.exports.model = mongoose.model("User", UserSchema);
