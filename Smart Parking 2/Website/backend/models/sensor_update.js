const mongoose = require('mongoose');

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
let SensorUpdateSchema = new mongoose.Schema({
    deviceId: {type: String, index: true},
    seqNumber: Number,
    duplicate: Boolean,
    available: Boolean,
    s1: { type: String, default: null },
    s2: { type: String, default: null },
    optional_data: { type: String, default: null },
    date: {
        type: Date,
        default: Date.now
    },
});
module.exports.schema = SensorUpdateSchema;
module.exports.model = mongoose.model("SensorUpdate", SensorUpdateSchema);
