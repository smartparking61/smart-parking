const mongoose = require('mongoose');
const SensorUpdate = require('./sensor_update');
const Point = require('./point');

let ParkingSensorSchema = new mongoose.Schema({
    sigfoxId: {type: String, index: true, unique: true},
    lastUpdate: SensorUpdate.schema,
    location: {
        type: Point.schema,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});
ParkingSensorSchema.index({ location: "2dsphere" });
module.exports.schema = ParkingSensorSchema;
module.exports.model = mongoose.model("ParkingSensor", ParkingSensorSchema);
