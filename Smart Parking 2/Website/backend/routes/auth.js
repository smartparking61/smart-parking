const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const config = require('../config');


const User = require('../models/user').model;

// let user = User({username: 'bartmachielsen', password:bcrypt.hashSync('Test', 10)});
// user.save().then(added => {}).catch(error => console.log(error));

/* GET users listing. */
router.post('/login', function(req, res, next) {
    if (!req.body || !req.body.username || !(req.body.password)) {
        return res.status(401).send({error: "Missing body"})
    }

    User.findOne({username: req.body.username}, function (error, user) {
        if (error) {
            console.error(error);
            return next(error);
        }
        if (!user) {
            return res.status(401).send({error: "User not found/password not match!"})
        }

        bcrypt.compare(req.body.password, user.password, function (error, same) {
            if (error) {
                return next(error);
            }

            if (error || !same) {
                return res.status(401).send({error: "User not found/password not match!"})
            }

            let access_token = jsonwebtoken.sign({
                    data: user
                }, config.jsonWebSigning, {expiresIn: '1y'}
            );

            return res.status(200).send({
                access_token: access_token
            });
        })


    });
});

module.exports.validate_webtoken = function (req, res, next) {
    if (req.method === 'OPTIONS') {
        return next();
    }
   let token = req.headers['x-access-token'];
   console.log(req.headers);

   if (!token) {
       return res.status(401).send({error: "No or invalid access token provided"});
   }

   jsonwebtoken.verify(token, config.jsonWebSigning, function (error, decoded) {
       if (error || !decoded) {
           return res.status(401).send({error: "No or invalid access token provided"});
       }
       res.user = decoded;
       return next()
   })
};

module.exports.router = router;
