const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const ParkingSensor = require('../models/parking_sensor').model;


function createSensor (res, sigfoxId, lat, lng) {
    ParkingSensor.create({
        sigfoxId: sigfoxId,
        location: {
            type: 'Point',
            coordinates: [lng, lat]
        }
    }, function (error, created) {
        if (error && error.code !== 11000) {
            console.error(error);
            return res.status(401).send({error: "Error in saving parking sensor"});
        }
        res.status(200).send({created: true, id: created._id});
    })
}

router.get('/all', function (req, res) {
    ParkingSensor.find({}, function (error, data) {
       res.status(200).send(data);
    });
});

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
router.get('/register', function (req, res) {
    if (!req.query.sigfoxId || !req.query.lat || !req.query.lng) {
        return res.status(401).send({error: "Missing params"});
    }
    createSensor(res, req.query.sigfoxId, req.query.lat, req.query.lng);
});

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
router.post('/register', function (req, res) {
    if (!req.body.sigfoxId || !req.body.lat || !req.body.lng) {
        return res.status(401).send({error: "Missing params"});
    }
    createSensor(res, req.body.sigfoxId, req.body.lat, req.body.lng);
});

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
router.get('/delete', function (req, res) {
    if (!req.query.sigfoxId) {
        return res.status(401).send({error: "Missing params"});
    }
    ParkingSensor.findOneAndDelete({sigfoxId: req.query.sigfoxId}, function (error) {
        if (error) {
            console.log(error);
            return res.status(500).send({error: "Deletion failed"})
        }
        res.status(200).send({message: "sensor deleted"})
    })
});

module.exports = router;
