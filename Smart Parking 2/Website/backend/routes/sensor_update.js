const express = require('express');
const router = express.Router();

const ParkingSensor = require('../models/parking_sensor').model;
const SensorUpdate = require('../models/sensor_update').model;

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
router.get('/', function (req, res) {
    if (!req.query.id || !req.query.seqNumber || !req.query.available) {
        console.warn("Missing params!");
        return res.status(401).send({error: "Missing params"});
    }
    let available = req.query.available;
    if (!req.query.available || (req.query.available !== "false" && req.query.available !== "true")) {
        available = false;
    }

    ParkingSensor.findOne({sigfoxId: req.query.id}, function(err, result) {
        if (err || !result) {
            if (err) {
                console.error(err);
            }
            console.warn("Device not found!");
            return res.status(404).send({success: "Device not found!"});
        }

        SensorUpdate.findOne(
            {
                date:{
                    $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000)
                },
                seqNumber: req.query.seqNumber,
                deviceId: result._id
            }, function (err, data) {
                if (err || data) {
                    console.error(err);
                    console.warn("Already registered!");
                    console.log(data);
                    return res.status(200).send({success: "Data already registered"});
                }

                if (!result.lastUpdate || result.lastUpdate.seqNumber < req.query.seqNumber) {

                    SensorUpdate.create(
                        {
                            deviceId: result._id,
                            seqNumber: req.query.seqNumber,
                            duplicate: req.query.duplicate,
                            available: available,
                            s1: req.query.s1,
                            s2: req.query.s2,
                            optional_data: req.query.optional_data
                        }, function (err, sensor_update) {
                            if (err) {
                                console.error(err);
                                console.warn("saving failed!");
                                return res.status(500).send({error: "Saving failed"});
                            }
                            ParkingSensor.find({_id: result._id}).updateOne({
                                lastUpdate: sensor_update
                            }, function (err, rows_effected) {
                                if (err) {
                                    console.error(err);
                                }
                                if (err || rows_effected === 0) {
                                    console.warn("Device not found!");
                                    return res.status(404).send({success: "Device not found!"});
                                }
                                console.warn("Data registered!");
                                res.status(200).send({success: "Data registered"});
                            });
                        });
                } else {
                    res.status(200).send({success: "Old message"});
                }
            });

    });
});

module.exports = router;
