const express = require('express');
const router = express.Router();

const ParkingSensor = require('../models/parking_sensor').model;

// id=device ,time, duplicate, seqNumber, detection, s1, s2, optional_data
router.get('/', function (req, res) {
    let location_search = {};

    if (req.query.lat && req.query.lng) {
        location_search = {
            location: {
                $near: {
                    $maxDistance: req.query.distance ? req.query.distance : 1000,
                    $geometry: {
                        type: "Point",
                        coordinates: [req.query.lng, req.query.lat]
                    }
                }
            }
        };
    }

    ParkingSensor.find(location_search).lean().exec(function (error, data) {
        if (error) {
            console.error(error);
            return res.status(500).send({error: "Database error"})
        }
        for(let element of data) {
            element.available = element.lastUpdate && element.lastUpdate.available;
            element.lastUpdate = element.lastUpdate ? element.lastUpdate.date : null;
            let location = {};
            location.lat = element.location.coordinates[1];
            location.lng = element.location.coordinates[0];
            element.location = location;
        }
        res.status(200).send(data);
    });
});

module.exports = router;
