import requests
import random

START_COORDINATES = [
    51.8187199986856, 4.265800676034814
]

END_COORDINATES = [
    51.95626798495873, 4.659100495370922
]


def random_coordinate():
    lat_diff = END_COORDINATES[0] - START_COORDINATES[0]
    lng_diff = END_COORDINATES[1] - START_COORDINATES[1]

    lat = START_COORDINATES[0] + (random.randint(0, 10000) / 10000.0) * lat_diff
    lng = START_COORDINATES[1] + (random.randint(0, 10000) / 10000.0) * lng_diff

    return lat, lng


def login():
    response = requests.post('http://localhost:3000/auth/login', json={
        "username": "bartmachielsen",
        "password": "Watson10"
    })
    return response.json().get('access_token')


def register(sigfoxId, lat, lng, access_token):
    response = requests.post('http://localhost:3000/secure/sensor/register', json={
        'sigfoxId': sigfoxId,
        'lat':lat,
        'lng': lng
    }, headers={"x-access-token": access_token})
    return response.json().get('id')


def generate_points(amount):
    access_token = login()
    ids = []
    for i in range(0, amount):
        lat, lng = random_coordinate()
        print(i, (lat, lng))
        register(i, lat, lng, access_token)
        ids.append(i)
    return ids


def run():
    ids = generate_points(100)


if __name__ == "__main__":
    run()
