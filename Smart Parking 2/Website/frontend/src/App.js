import React  from 'react'
import {Row, Col} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import MapComponent from './components/map';
import LoginComponent from './components/login'


function Navigator() {
    return (
        <Router>
            <div>
                <Row style={{paddingLeft:"10px"}}>
                    <Col md={2} style={{marginTop:"10px"}}>
                        <h4>SmartParking</h4>
                    </Col>
                    <Col md={1} style={{marginTop:"20px"}}>
                        <Link to="/" style={{textAlign: 'center'}}>Dashboard</Link>
                    </Col>
                    <Col md={1} style={{marginTop:"20px"}}>
                        <Link to="/admin" style={{textAlign: 'center'}}>Admin</Link>
                    </Col>
                </Row>

                <hr />

                <Route exact path="/" component={MapComponent} />
                <Route path="/admin" component={LoginComponent} />
            </div>
        </Router>
    );
}


export default Navigator;