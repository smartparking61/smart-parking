import React, { createRef, Component } from 'react'
import { Map, TileLayer, Marker } from 'react-leaflet';
import {geolocated} from 'react-geolocated';
import {availableIcon, unavailableIcon, manIcon} from '../icon';
import {Row, Col, Form, FormGroup, ControlLabel, FormControl, Button} from 'react-bootstrap';
import {backend_url} from '../conf';
var Spinner = require('react-spinkit');

const HERE_APP_ID = "jz0ZonSYOTz1n3blg1xJ";
const HERE_APP_CODE = "XT41A44pq88Y7vPA0ybw1A";


class MapComponent extends Component{
    constructor(props, context) {
        super(props, context);

        this.addressSubmit = this.addressSubmit.bind(this);
        this.searchAddress = this.searchAddress.bind(this);
        this.updateParkingSensors = this.updateParkingSensors.bind(this);
        this.setCurrentLocation = this.setCurrentLocation.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.setFocus = this.setFocus.bind(this);
        this.mapRef = createRef();
    }

    getParkingSensors(lat, lng, distance=1000) {
        return new Promise((resolve, reject) => {
            let params = "";
            if (lat && lng) {
                params = `?lat=${lat}&lng=${lng}&distance=${distance}`;
                this.setState({coords: {latitude: lat, longitude: lng}});
            }
            fetch(`${backend_url}/parking` + params).then(results => results.json()).then(data => {
                if (data.error) {
                    console.error(data);
                } else {
                    resolve(data);
                }
            }).catch(error => {console.error(error); reject(error)});
        });
    }

    updateParkingSensors(lat, lng, distance=1000) {
        this.getParkingSensors(lat, lng, distance).then(data => {
            this.setState({parking_sensors: data});
        });
    }

    update_map() {
        setTimeout(
            function() {
                this.updateParkingSensors();
                this.update_map();
            }.bind(this),
            1000
        );
    }

    componentDidMount() {
        this.updateParkingSensors();
        this.update_map();
    }

    DEFAULT_COORDINATES = {
        latitude: 51.91851106272676,
        longitude: 4.444791423814677
    };

    state = {
        zoom: 17,
        coords: null,
        nearby: []
    };

    searchAddress(e) {
        e.preventDefault();
        fetch(`https://geocoder.api.here.com/6.2/geocode.json?app_id=${HERE_APP_ID}&app_code=${HERE_APP_CODE}&searchtext=${this.state.address}`
        ).then(results => results.json()).then(data => {
            let addresses = [];
            for (let item of data.Response.View) {
                for (let result of item.Result) {
                    addresses.push(result.Location)
                }
            }
            this.setState({addresses: addresses});
        }).catch(error => console.error(error));
    }

    addressSubmit(e) {
        this.setState({ address: e.target.value });
    }

    handleClick(e){
        if (e.latlng) {
            this.getParkingSensors(e.latlng.lat, e.latlng.lng, 1000).then(data => {
                this.setState({nearby: data});
            });
        }
    };

    setCurrentLocation() {
        this.setState({coords: this.props.coords});
    }

    setFocus(lat, lng) {
        this.getParkingSensors(lat, lng, 1000).then(data => {
            this.setState({nearby: data});
        });
    }

    render() {
        if (!this.props.isGeolocationAvailable) {
            return (
                <div>
                    <div style={{marginTop: "15%", display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <h2>Geolocation disabled</h2>
                    </div>
                    <div style={{marginTop: "5%", display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <Spinner name='ball-clip-rotate-multiple' />
                    </div>
                </div>
            )
        }
        let userLocation;
        let locationButton;
        if (this.props && this.props.coords) {
            userLocation = (
                <Marker icon={manIcon} position={[this.props.coords.latitude, this.props.coords.longitude]}/>
            );
            locationButton = (<button onClick={this.setCurrentLocation}>My location</button>);
        }
        let position;
        if (!this.state.coords && this.props.coords) {
            position = [this.props.coords.latitude, this.props.coords.longitude];
        } else if (this.state.coords) {
            position = [this.state.coords.latitude, this.state.coords.longitude];
        } else {
            position = [this.DEFAULT_COORDINATES.latitude, this.DEFAULT_COORDINATES.longitude]
        }

        const markers = [];
        if (this.state.parking_sensors) {
            for (let parking_sensor of this.state.parking_sensors) {
                markers.push(<Marker icon={parking_sensor.available? availableIcon: unavailableIcon} key={parking_sensor.sigfoxId} position={[parking_sensor.location.lat, parking_sensor.location.lng]} />);
            }
        }

        let addresses = [];

        if (this.state.addresses !== null && this.state.addresses !== undefined) {
            if (!this.state.addresses) {
                addresses = (<p>Geen resultaten gevonden</p>)
            } else {
                for (let address of this.state.addresses) {
                    addresses.push(
                        <p key={address.LocationId} onClick={() => {this.setFocus(address.DisplayPosition.Latitude, address.DisplayPosition.Longitude)}}>{address.Address.Label}</p>
                    )
                }
            }
        }

        let nearby = [];
        if (this.state.nearby !== null && this.state.nearby !== undefined) {
            for (let index in this.state.nearby) {
                let near = this.state.nearby[index];
                nearby.push(
                    <div key={near._id} style={{marginBottom:"10px", color: near.available ? "green": "black"}} onClick={ () => this.setState({coords:{latitude:near.location.lat, longitude: near.location.lng}})}>
                        <h5>{index}: Parkingspot</h5>
                        <i>{near.location.lat}, {near.location.lng}</i>
                        <br/>
                        <br/>
                    </div>
                )
            }
        }

        return (
            <Row className="show-grid">
                <Col md={9} >
                    <Map center={position}
                         zoom={this.state.zoom}
                         style={{height: "100vh"}}
                         onClick={this.handleClick}
                         length={4}
                         ref={this.mapRef}>
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {/*<MarkerClusterGroup>*/}
                        {markers}
                        {/*</MarkerClusterGroup>*/}
                        {userLocation}
                    </Map>
                </Col>
                <Col md={3} style={{padding:"50px"}}>
                    <h2>Search</h2>

                    {locationButton}
                    <br/>
                    <Form onSubmit={this.searchAddress}>
                        <FormGroup
                            onSubmit={this.searchAddress}
                        >
                            <ControlLabel>Look up address location</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.value}
                                placeholder="Enter text"
                                onChange={this.addressSubmit}
                            />
                        </FormGroup>
                        <Button
                            block
                            bsSize="small"
                            type="submit"
                             value="Submit">Search</Button>
                    </Form>
                    <div style={{marginTop: "10px", padding: "20px"}}>
                        {addresses}
                    </div>
                    <div style={{marginTop: "10px", padding: "20px"}}>
                        {nearby}
                    </div>
                </Col>
            </Row>
        )
    }
}

export default geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    userDecisionTimeout: 500,
})(MapComponent);



// export default geolocated;