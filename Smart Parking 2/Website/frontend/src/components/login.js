import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import AdminComponent from "./admin";
import {backend_url} from '../conf';


export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            authentication_token: sessionStorage.getItem('authentication_token')
        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = event => {
        fetch(`${backend_url}/auth/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.email,
                password: this.state.password,
            })
        }).then(async response => {
            response = JSON.parse(await response.text());
            if (response.error) {
                this.setState({error: response.error});
            } else {
                console.log(response);
                this.setState({error: null, authentication_token: response.access_token});
                sessionStorage.setItem('authentication_token', response.access_token);
            }
        }).catch(error => console.error(error));
        event.preventDefault();
    };

    render() {
        if (this.state.authentication_token) {
            return <AdminComponent authentication_token={this.state.authentication_token} onlogOut={() => {this.setState({authentication_token: null})}}/>
        }
        let error;
        if (this.state.error) {
            error = <p style={{color:"red"}}>{this.state.error}</p>
        }
        return (
            <div className="Login" style={{margin:"100px", maxWidth: "350px"}}>
                {error}
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}