import {Component} from "react";
import React from "react";
import {Col, Row, Table, Form, FormGroup, FormControl, Button, ControlLabel} from "react-bootstrap";
import {backend_url} from '../conf';


class RegisterComponent extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {};

        this.submitRegister = this.submitRegister.bind(this);
    }

    submitRegister(e) {
        e.preventDefault();
        this.props.onRegister(this.state);
        this.setState({sigfoxId: '', latitude: '', longitude:''})
    }


    render () {
        return (
            <Form horizontal onSubmit={this.submitRegister}>
                <FormGroup controlId="formHorizontalEmail">
                    <Col componentClass={ControlLabel} sm={3}>
                        SigfoxID
                    </Col>
                    <Col sm={8}>
                        <FormControl type="text" placeholder="" value={this.state.sigfoxId} onChange={e => this.setState({sigfoxId: e.target.value})}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalLatitude">
                    <Col componentClass={ControlLabel} sm={3}>
                        Latitude
                    </Col>
                    <Col sm={8}>
                        <FormControl type="decimal" placeholder="" value={this.state.latitude} onChange={e => this.setState({lat: e.target.value})}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalLongitude">
                    <Col componentClass={ControlLabel} sm={3}>
                        Longitude
                    </Col>
                    <Col sm={8}>
                        <FormControl type="decimal" placeholder="" value={this.state.longitude} onChange={e => this.setState({lng: e.target.value})}/>
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button type="submit">Register</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}


export default class AdminComponent extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {};

        this.get_sensors = this.get_sensors.bind(this);
        this.register = this.register.bind(this);
        this.delete_sensor = this.delete_sensor.bind(this);
    }

    get_sensors () {
        fetch(`${backend_url}/secure/sensor/all`, {
            headers: {
                'x-access-token': this.props.authentication_token
            }
        }).then(async data => {
            let response = await data.json();
            if (response.error) {
                console.log(response.error);
                if (this.props.onlogOut) {
                    this.props.onlogOut();
                }
            } else {
                this.setState({sensors: response});
            }
        }).catch(error => {
            console.log(error);
            this.props.authentication_token = null;
        });
    }

    register(state) {
        console.log(state);
        fetch(`${backend_url}/secure/sensor/register`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.props.authentication_token
            },
            body: JSON.stringify(state)
        }).then(async data => {
            let response = await data.json();
            console.log(response);
            this.get_sensors();
        });

    }

    delete_sensor(sigfoxId) {

        fetch(`${backend_url}/secure/sensor/delete?sigfoxId=${sigfoxId}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.props.authentication_token
            }
        }).then(async data => {
            let response = await data.json();
            console.log(response);
            this.get_sensors();
        });
    }

    componentDidMount () {
        this.get_sensors();
    }

    render () {
        let sensors = [];

        if (this.state && this.state.sensors) {
            for (let index in this.state.sensors) {
                let sensor = this.state.sensors[index];
                let sensor_data = null;
                if (sensor.lastUpdate && (sensor.lastUpdate.s1 || sensor.lastUpdate.s2 || sensor.lastUpdate.optional_data)) {
                    sensor_data = <ul>
                        <li>Sensor data 1: &nbsp; {sensor.lastUpdate.s1}</li>
                        <li>Sensor data 2: &nbsp; {sensor.lastUpdate.s2}</li>
                        <li>Other data: &nbsp; {sensor.lastUpdate.optional_data}</li>
                    </ul>
                }
                sensors.push((
                    <tr key={index}>
                        <td>{index}</td>
                        <td>{sensor.sigfoxId}</td>
                        <td>{sensor.date}</td>
                        <td>{sensor.location.coordinates[1]}</td>
                        <td>{sensor.location.coordinates[0]}</td>
                        <td>{sensor.lastUpdate ? sensor.lastUpdate.date: null}</td>
                        <td>{sensor.lastUpdate ? String(sensor.lastUpdate.available) : null}</td>
                        <td>{sensor.lastUpdate ? String(sensor.lastUpdate.seqNumber) : null}</td>
                        <td>{sensor_data}</td>
                        <td>
                            <Button onClick={()=>{this.delete_sensor(sensor.sigfoxId);}}>Delete</Button>
                        </td>
                    </tr>

                ))
            }
        }

        return (
            <Row className="show-grid" style={{margin:"50px"}}>
                <Col md={9} style={{padding:"20px"}}>
                    <h1>Active Sensors</h1>

                    <Table striped bordered condensed hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>SigfoxID</th>
                            <th>Registered</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Last Update</th>
                            <th>Available</th>
                            <th>SeqNumber</th>
                            <th>SensorData</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            {sensors}
                        </tbody>
                    </Table>
                </Col>
                <Col md={3} style={{padding:"20px"}}>
                    <h1>Register Sensor</h1>
                    <RegisterComponent onRegister={this.register}/>
                </Col>
            </Row>
        )
    }
}
