import L from 'leaflet';

const availableIcon = new L.Icon({
    iconUrl: require('./img/green.svg'),
    iconRetinaUrl: require('./img/green.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(25, 40),
    // className: 'leaflet-div-icon'
});
const unavailableIcon = new L.Icon({
    iconUrl: require('./img/black.svg'),
    iconRetinaUrl: require('./img/black.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(20, 35),
    // className: 'leaflet-div-icon'
});
const manIcon = new L.Icon({
    iconUrl: require('./img/man.svg'),
    iconRetinaUrl: require('./img/man.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(30, 45),
    // className: 'leaflet-div-icon'
});

export { unavailableIcon, availableIcon, manIcon };